const httpStatus = require('http-status');

/**
 * users
 * @public
 */
exports.users = async (req, res, next) => {
  res.status(httpStatus.OK);
  return res.json({
    responseCode: httpStatus.OK,
    responseMessage: 'OK',
    response: {}
  });
};
